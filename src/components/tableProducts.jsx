import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './../css/tableProduct.css';

function Table(props) {
  const [dataT, setData] = useState({});
  useEffect(()=>{

      axios.get('http://192.168.64.2/hardwarestore/insert.php').then(res => 
      {
        console.log(res.data);
          setData(res.data);
          console.log(dataT);
          
      }); 


},[]);
/*const listItems = data.forEach(element => {
  <tr>
        
            <th scope="row">{element.Name}</th>
            <td>{element.Name}</td>
            <td>{element.Name}</td>
            <td>{element.Name}</td>
            <td>{element.Name}</td>
            <td>{element.Name}</td>
            <td>{element.Name}</td>
            </tr>
});
*/

    return (
      <div>
        <h1 id="title">Inventario</h1>
        <div class="input-group flex-nowrap">
            <input type="text" class="form-control" placeholder="search" aria-label="Username" aria-describedby="addon-wrapping"/>
            <span class="input-group-text" id="addon-wrapping">Filter</span>
        </div>
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripción</th>
            <th scope="col">Proveedor</th>
            <th scope="col">Existencia</th>
            <th scope="col">Precio publico</th>
            <th scope="col">Precio Proveedor</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>Otto</td>
            <td>Otto</td>
            <td>Otto</td>
            <td>@mdo</td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>Thornton</td>
            <td>Thornton</td>
            <td>Thornton</td>
            <td>@fat</td>
            </tr>
            <tr>
            <th scope="row">3</th>
            <td>Larry the Bird</td>
            <td>Larry the Bird</td>
            <td>Larry the Bird</td>
            <td>Larry the Bird</td>
            <td>Larry the Bird</td>
            <td>@twitter</td>
            </tr>
        </tbody>
        </table>
      </div>
    );
}

export default Table;