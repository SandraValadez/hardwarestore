import React from 'react';
import "./../css/product.css";
import axios from 'axios';

function Product(props) {


    return (
      <div id="borderProduct">
        <div className="row">
            <div class="col-12 ">
                <label for="exampleFormControlInput1" class="form-label">Codigo de barras</label>
                <input class="form-control" id="exampleFormControlInput1" />
            </div>   
        </div>
        <div className="row"> 
            <div class="col-12">
                <label for="exampleFormControlInput1" class="form-label">Nombre</label>
                <input  class="form-control" id="exampleFormControlInput1"/>
            </div> 
        </div>
        <div className="row">
            <div class="col-12">
                <label for="exampleFormControlTextarea1" class="form-label">Descripción</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
        </div>
        
        <div className="row">
            <div class="col-3">
                <label for="exampleFormControlInput1" class="form-label">Fecha de ultima compra</label>
                <input type="date" class="form-control" id="exampleFormControlInput1"  />
            </div>
            <div class="col-3">
                <label for="exampleFormControlInput1" class="form-label">Proveedor</label>
                <input type="email" class="form-control" id="exampleFormControlInput1"  />
            </div>
            <div class="col-3">
                <label for="exampleFormControlInput1" class="form-label">Existencia</label>
                <input type="email" class="form-control" id="exampleFormControlInput1"  />
            </div>
            <div class="col-3">
                <label for="exampleFormControlInput1" class="form-label">Minimo de existencia</label>
                <input type="email" class="form-control" id="exampleFormControlInput1"  />
            </div>
        </div>
        <div className="row">
            <div class="col-6">
                <label for="exampleFormControlInput1" class="form-label">Precio al publico</label>
                <input type="email" class="form-control" id="exampleFormControlInput1"  />
            </div>
            <div class="col-6">
                <label for="exampleFormControlInput1" class="form-label">Precio proveedor</label>
                <input type="email" class="form-control" id="exampleFormControlInput1"  />
            </div>
        </div>
        <div className="row space" >
        <div className="col-2"></div>
        <button type="button" class="btn btn-primary col-4">Guardar</button>
        <button type="button" class="btn btn-secondary col-4">Cancelar</button>
        </div>
      </div>
    );
}

export default Product;