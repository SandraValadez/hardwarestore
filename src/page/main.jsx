import React, { useEffect, useState } from 'react';
import Table from '../components/tableProducts';
import Product from '../components/Product';
import axios from 'axios';

function Main(props) {
    
    return (
      <div>
      {/* barra de navegacion */}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Vagarin's</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Punto de venta</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Nuevo producto</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>


    {/* router
     <Product></Product>
    */}
    <div className="container">
     
      
     <Table></Table>

    </div>
        
      </div>
    );
}

export default Main;